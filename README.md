# RecyclerViewProfiler

6 разных RecyclerView адаптеров для анализирования с профайлером. \
Адаптеры: 
1. Создание холдеров с LayoutInflater.
1. Создание холдеров с LayoutInflater + переопределен getItemId.
1. Создание холдеров с dataBinding, биндинг с viewModel.
1. Создание холдеров с dataBinding, биндинг с viewModel + переопределен getItemId.
1. Холдеры описаны программно, без xml.
1. Холдеры описаны программно, без xml + переопределен getItemId.