package com.example.recyclerviewprofiler.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop

@BindingAdapter("image")
fun ImageView.setImage(id: Int?){
    id?.let {
        Glide.with(context)
            .load(context.getDrawable(id))
            .transform(CircleCrop())
            .into(this)
    }
}