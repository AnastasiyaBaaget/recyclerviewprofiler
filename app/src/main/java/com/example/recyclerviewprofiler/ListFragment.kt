package com.example.recyclerviewprofiler

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewprofiler.adapters.*
import kotlin.random.Random

class ListFragment : Fragment() {

    companion object{
        const val FIRST = 1
        const val SECOND = 2
        const val THIRD = 3
        const val FOURTH = 4
        const val FIFTH = 5
        const val SIXTH = 6
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let {

            val data = createData(120)

            val adapter =
                when(ListFragmentArgs.fromBundle(arguments!!).adapter) {
                    FIRST -> {
                        FirstAdapter(it)

                    }
                    SECOND -> SecondAdapter(it)
                    THIRD -> ThirdAdapter(it)
                    FOURTH -> FourthAdapter(it)
                    FIFTH -> FifthAdapter()
                    else -> {
                        SixthAdapter()
                    }
                }

            val divider = DividerItemDecoration(context, LinearLayout.VERTICAL)
            divider.setDrawable(resources.getDrawable(R.drawable.divider_empty, null))
            val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
            recyclerView?.adapter = adapter
            recyclerView?.addItemDecoration(divider)
            adapter.data = data
        }
    }

    private fun createData(size: Int): List<AdapterData> {
        val images = listOf(
            R.drawable.butterfly,
            R.drawable.grapefruit,
            R.drawable.thumb,
            R.drawable.test,
            R.drawable.ruby
        )

        val result = mutableListOf<AdapterData>()
        for (i in 0..size) {
            result.add(AdapterData(
                i.toLong(),
                "title",
                "body",
                "text1",
                "text2",
                images[Random.nextInt(images.size)],
                HolderType.values()[i % HolderType.values().size]
            ))
        }
        return result
    }
}

data class AdapterData(
    val id: Long,
    val title: String,
    val body: String,
    val text1: String,
    val text2: String,
    val imgId: Int?,
    val type: HolderType)
