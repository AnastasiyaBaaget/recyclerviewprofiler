package com.example.recyclerviewprofiler

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.recyclerviewprofiler.databinding.FragmentMenuBinding

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(inflater, container, false)
        binding.firstButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.FIRST)))
        }
        binding.secondButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.SECOND)))
        }
        binding.thirdButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.THIRD)))
        }
        binding.fourthButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.FOURTH)))
        }
        binding.fifthButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.FIFTH)))
        }

        binding.sixthButton.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_listFragment, bundleOf(Pair("adapter", ListFragment.SIXTH)))
        }

        return binding.root
    }

}
