package com.example.recyclerviewprofiler

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
}

enum class HolderType{
    first, second, third,fourth, fifth, sixth, seventh, eighth, ninth, tenth;
}