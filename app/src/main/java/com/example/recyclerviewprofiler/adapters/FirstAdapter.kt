package com.example.recyclerviewprofiler.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.recyclerviewprofiler.AdapterData
import com.example.recyclerviewprofiler.HolderType
import com.example.recyclerviewprofiler.R
import com.example.recyclerviewprofiler.ViewHolder

class FirstAdapter(val context : Context): Adapter() {

    override var data = listOf<AdapterData>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType) {
                HolderType.first.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_first, parent, false)
                    FirstHolder(view)
                }
                HolderType.second.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_second, parent, false)
                    SecondHolder(view)
                }
                HolderType.third.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_third, parent, false)
                    ThirdHolder(view)
                }
                HolderType.fourth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_fourth, parent, false)
                    FourthHolder(view)
                }
                HolderType.fifth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_fifth, parent, false)
                    FifthHolder(view)
                }
                HolderType.sixth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_sixth, parent, false)
                    SixthHolder(view)
                }
                HolderType.seventh.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_seventh, parent, false)
                    SeventhHolder(view)
                }
                HolderType.eighth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_eighth, parent, false)
                    EigthHolder(view)
                }
                HolderType.ninth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_ninth, parent, false)
                    NinthHolder(view)
                }
                HolderType.tenth.ordinal -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_tenth, parent, false)
                    TenthHolder(view)
                }

                else -> {
                    val view = LayoutInflater.from(context).inflate(R.layout.item_second, parent, false)
                    SecondHolder(view)
                }
            }
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            HolderType.first.ordinal -> (holder as FirstHolder).bind(data[position])
            HolderType.second.ordinal -> (holder as SecondHolder).bind(data[position])
            HolderType.third.ordinal -> (holder as ThirdHolder).bind(data[position])
            HolderType.fourth.ordinal -> (holder as FourthHolder).bind(data[position])
            HolderType.fifth.ordinal -> (holder as FifthHolder).bind(data[position])
            HolderType.sixth.ordinal -> (holder as SixthHolder).bind(data[position])
            HolderType.seventh.ordinal -> (holder as SeventhHolder).bind(data[position])
            HolderType.eighth.ordinal -> (holder as EigthHolder).bind(data[position])
            HolderType.ninth.ordinal -> (holder as NinthHolder).bind(data[position])
            HolderType.tenth.ordinal -> (holder as TenthHolder).bind(data[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.ordinal
    }

    class FirstHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body).text = data.body
            view.findViewById<TextView>(R.id.text1).text = data.text1
            view.findViewById<TextView>(R.id.text2).text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }

        }
    }

    class SecondHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body).text = data.body
            view.findViewById<TextView>(R.id.text1).text = data.text1

            data.imgId?.let {
                view.findViewById<ImageView>(R.id.image)?.setImageDrawable(view.context.getDrawable(data.imgId))
            }
        }
    }

    class ThirdHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body).text = data.body
            view.findViewById<TextView>(R.id.text1).text = data.text1

            view.findViewById<ImageButton>(R.id.imageButton).setOnClickListener {
                Toast.makeText(it.context, "HI Third Holder!", Toast.LENGTH_SHORT).show()
            }
            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class FourthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body).text = data.body
            view.findViewById<TextView>(R.id.text1).text = data.text1
            view.findViewById<TextView>(R.id.text2).text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class FifthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body).text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                view.findViewById<ImageView>(R.id.image)?.setImageDrawable(view.context.getDrawable(data.imgId))
            }

        }
    }

    class SixthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body)?.text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class SeventhHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body)?.text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class EigthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body)?.text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class NinthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body)?.text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }

    class TenthHolder(val view: View): ViewHolder(view) {
        fun bind(data: AdapterData) {
            view.findViewById<TextView>(R.id.title).text = data.title
            view.findViewById<TextView>(R.id.body)?.text = data.body
            view.findViewById<TextView>(R.id.text1)?.text = data.text1
            view.findViewById<TextView>(R.id.text2)?.text = data.text2

            data.imgId?.let {
                Glide.with(view)
                    .load(view.context.getDrawable(data.imgId))
                    .transform(CircleCrop())
                    .into(view.findViewById(R.id.image))
            }
        }
    }
}