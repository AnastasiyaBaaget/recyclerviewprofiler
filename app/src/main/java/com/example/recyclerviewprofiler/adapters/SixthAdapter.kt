package com.example.recyclerviewprofiler.adapters

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.setPadding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.recyclerviewprofiler.AdapterData
import com.example.recyclerviewprofiler.HolderType
import com.example.recyclerviewprofiler.R
import com.example.recyclerviewprofiler.ViewHolder
import com.example.recyclerviewprofiler.util.setImage

class SixthAdapter: Adapter() {
    override var data = listOf<AdapterData>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            HolderType.first.ordinal -> FifthAdapter.FirstHolder.create(parent)
            HolderType.second.ordinal -> FifthAdapter.SecondHolder.create(parent)
            HolderType.third.ordinal -> FifthAdapter.ThirdHolder.create(parent)
            HolderType.fourth.ordinal -> FifthAdapter.FourthHolder.create(parent.context)
            HolderType.fifth.ordinal -> FifthAdapter.FifthHolder.create(parent.context)
            HolderType.sixth.ordinal -> FifthAdapter.SixthHolder.create(parent.context)
            HolderType.seventh.ordinal -> FifthAdapter.SeventhHolder.create(parent.context)
            HolderType.eighth.ordinal -> FifthAdapter.EighthHolder.create(parent.context)
            HolderType.ninth.ordinal -> FifthAdapter.NinthHolder.create(parent.context)
            else -> FifthAdapter.TenthHolder.create(parent.context)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            HolderType.first.ordinal -> (holder as FifthAdapter.FirstHolder).bind(data[position])
            HolderType.second.ordinal -> (holder as FifthAdapter.SecondHolder).bind(data[position])
            HolderType.third.ordinal -> (holder as FifthAdapter.ThirdHolder).bind(data[position])
            HolderType.fourth.ordinal -> (holder as FifthAdapter.FourthHolder).bind(data[position])
            HolderType.fifth.ordinal -> (holder as FifthAdapter.FifthHolder).bind(data[position])
            HolderType.sixth.ordinal -> (holder as FifthAdapter.SixthHolder).bind(data[position])
            HolderType.seventh.ordinal -> (holder as FifthAdapter.SeventhHolder).bind(data[position])
            HolderType.eighth.ordinal -> (holder as FifthAdapter.EighthHolder).bind(data[position])
            HolderType.ninth.ordinal -> (holder as FifthAdapter.NinthHolder).bind(data[position])
            else -> (holder as FifthAdapter.TenthHolder).bind(data[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.ordinal
    }

    override fun getItemId(position: Int): Long {
        return data[position].id
    }

    class FirstHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(parent: ViewGroup): FirstHolder {
                val result = LinearLayout(parent.context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(parent.context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(50, 50)

                val layout1 = LinearLayout(parent.context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(parent.context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val body = TextView(parent.context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.addView(title)
                layout1.addView(body)

                val layout2 = LinearLayout(parent.context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.VERTICAL
                val text1 = TextView(parent.context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(parent.context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                result.addView(image)
                result.addView(layout1)
                result.addView(layout2)
                return FirstHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2
            view.findViewById<ImageView>(imageId)?.setImage(data.imgId)
        }
    }

    class SecondHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(parent: ViewGroup): SecondHolder {
                val result = LinearLayout(parent.context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(parent.context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(50, 50)

                val layout1 = LinearLayout(parent.context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(parent.context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val body = TextView(parent.context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.addView(title)
                layout1.addView(body)

                val layout2 = LinearLayout(parent.context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val text1 = TextView(parent.context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(parent.context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                result.addView(image)
                layout1.addView(layout2)
                result.addView(layout1)
                return SecondHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2
            view.findViewById<ImageView>(imageId)?.setImage(data.imgId)
        }
    }

    class ThirdHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(parent: ViewGroup): ThirdHolder {
                val result = LinearLayout(parent.context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(parent.context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(50, 50)

                val frameLayout = FrameLayout(parent.context)
                frameLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

                val layout1 = LinearLayout(parent.context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(parent.context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val body = TextView(parent.context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.addView(title)
                layout1.addView(body)

                val layout2 = LinearLayout(parent.context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val text1 = TextView(parent.context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(parent.context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                val button = ImageButton(parent.context)
                button.setImageResource(R.drawable.ic_more_vert_24px)
                button.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                result.addView(image)
                layout1.addView(layout2)
                frameLayout.addView(layout1)
                result.addView(frameLayout)
                result.addView(button)
                return ThirdHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2

            view.findViewById<ImageView>(imageId)?.let {
                Glide.with(view.context).load(data.imgId).into(it)
            }
        }
    }

    class FourthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): FourthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(50, 50)

                val frameLayout = FrameLayout(context)
                frameLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.addView(title)
                layout1.addView(body)

                val layout2 = LinearLayout(context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                val layout3 = LinearLayout(context)
                layout3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.setBackgroundColor(Color.GRAY)
                layout3.orientation = LinearLayout.VERTICAL
                val view1 = View(context)
                view1.layoutParams = ViewGroup.LayoutParams(70,70)
                val view2 = View(context)
                view2.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70)
                view2.setBackgroundColor(Color.YELLOW)
                layout3.addView(view1)
                layout3.addView(view2)

                val button = ImageButton(context)
                button.setImageResource(R.drawable.ic_more_vert_24px)
                button.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                result.addView(image)
                layout1.addView(layout2)
                frameLayout.addView(layout1)
                result.addView(frameLayout)
                result.addView(layout3)
                result.addView(button)
                return FourthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2

            view.findViewById<ImageView>(imageId)?.let {
                Glide.with(view.context).load(data.imgId).into(it)
            }
        }
    }

    class FifthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): FifthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(50, 50)

                val frameLayout = FrameLayout(context)
                frameLayout.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.addView(title)
                layout1.addView(body)

                val layout2 = LinearLayout(context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                val layout3 = LinearLayout(context)
                layout3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.setBackgroundColor(Color.GRAY)
                layout3.orientation = LinearLayout.VERTICAL
                val view1 = View(context)
                view1.layoutParams = ViewGroup.LayoutParams(70,70)
                val view2 = View(context)
                view2.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 70)
                view2.setBackgroundColor(Color.YELLOW)
                layout3.addView(view1)
                layout3.addView(view2)

                val button = ImageButton(context)
                button.setImageResource(R.drawable.ic_more_vert_24px)
                button.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                result.addView(image)
                layout1.addView(layout2)
                frameLayout.addView(layout1)
                result.addView(layout3)
                result.addView(frameLayout)
                result.addView(button)
                return FifthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2

            view.findViewById<ImageView>(imageId)?.let {
                Glide.with(view.context).load(data.imgId).into(it)
            }
        }
    }

    class SixthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): SixthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(100, 100)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val view1 = View(context)
                view1.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,70)
                view1.setBackgroundColor(Color.LTGRAY)

                val layout2 = LinearLayout(context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val title = TextView(context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val view2 = View(context)
                view2.layoutParams = ViewGroup.LayoutParams(100, 70)
                view2.setBackgroundColor(Color.YELLOW)
                layout2.addView(title)
                layout2.addView(view2)

                layout1.addView(view1)
                layout1.addView(layout2)

                val layout3 = LinearLayout(context)
                layout3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.setBackgroundColor(Color.GRAY)
                layout3.orientation = LinearLayout.VERTICAL
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.addView(body)
                layout3.addView(text1)
                layout3.addView(text2)

                result.addView(image)
                result.addView(layout1)
                result.addView(layout3)
                return SixthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2

            view.findViewById<ImageView>(imageId)?.let {
                Glide.with(view.context).load(data.imgId).into(it)
            }
        }
    }

    class SeventhHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): SeventhHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(100, 100)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val view1 = View(context)
                view1.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,70)
                view1.setBackgroundColor(Color.LTGRAY)

                val layout2 = LinearLayout(context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val title = TextView(context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val view2 = View(context)
                view2.layoutParams = ViewGroup.LayoutParams(100, 70)
                view2.setBackgroundColor(Color.YELLOW)
                layout2.addView(title)
                layout2.addView(view2)

                layout1.addView(view1)
                layout1.addView(layout2)

                val layout3 = LinearLayout(context)
                layout3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.setBackgroundColor(Color.GRAY)
                layout3.orientation = LinearLayout.VERTICAL
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout3.addView(body)
                layout3.addView(text1)
                layout3.addView(text2)

                result.addView(image)
                result.addView(layout3)
                result.addView(layout1)
                return SeventhHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2

            view.findViewById<ImageView>(imageId)?.let {
                Glide.with(view.context).load(data.imgId).into(it)
            }
        }
    }

    class EighthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): EighthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(150, 150)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL
                val title = TextView(context)
                titleId = View.generateViewId()
                title.id = titleId
                title.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
                title.setTextColor(Color.BLACK)
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.id = bodyId
                body.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

                val layout2 = LinearLayout(context)
                layout2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.orientation = LinearLayout.HORIZONTAL
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.addView(text1)
                layout2.addView(text2)

                result.addView(image)
                layout1.addView(layout2)
                layout1.addView(title)
                layout1.addView(body)
                result.addView(layout1)
                return EighthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2
            view.findViewById<ImageView>(imageId)?.setImage(data.imgId)
        }
    }

    class NinthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): NinthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(150, 150)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL

                val layout = LinearLayout(context)
                val title = TextView(context)
                titleId = View.generateViewId()
                title.apply {
                    id = titleId
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
                    setTextColor(Color.BLACK)
                }
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.apply {
                    id = bodyId
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                }
                layout.apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    orientation = LinearLayout.HORIZONTAL
                    addView(title)
                    addView(body)
                }

                val layout2 = LinearLayout(context)
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    orientation = LinearLayout.HORIZONTAL
                    addView(text1)
                    addView(text2)
                }

                val view = View(context)
                view.apply {
                    layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100)
                    view.setBackgroundColor(Color.MAGENTA)
                }

                result.addView(image)
                layout1.addView(layout)
                layout1.addView(layout2)
                layout1.addView(view)
                result.addView(layout1)
                return NinthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2
            view.findViewById<ImageView>(imageId)?.setImage(data.imgId)
        }
    }

    class TenthHolder(val view: View) : ViewHolder(view) {
        companion object{
            fun create(context: Context): TenthHolder {
                val result = LinearLayout(context)
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                result.layoutParams = lp
                result.orientation = LinearLayout.HORIZONTAL
                result.setBackgroundResource(R.drawable.bg_item)
                result.setPadding(20)

                val image = ImageView(context)
                imageId = View.generateViewId()
                image.id = imageId
                image.layoutParams = ViewGroup.LayoutParams(150, 150)

                val layout1 = LinearLayout(context)
                layout1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout1.orientation = LinearLayout.VERTICAL

                val layout = LinearLayout(context)
                val title = TextView(context)
                titleId = View.generateViewId()
                title.apply {
                    id = titleId
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 16.0f)
                    setTextColor(Color.BLACK)
                }
                val body = TextView(context)
                bodyId = View.generateViewId()
                body.apply {
                    id = bodyId
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                }
                layout.apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    orientation = LinearLayout.HORIZONTAL
                    addView(title)
                    addView(body)
                }

                val layout2 = LinearLayout(context)
                val text1 = TextView(context)
                textId = View.generateViewId()
                text1.id = textId
                text1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val text2 = TextView(context)
                text2Id = View.generateViewId()
                text2.id = text2Id
                text2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                layout2.apply {
                    layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    orientation = LinearLayout.HORIZONTAL
                    addView(text1)
                    addView(text2)
                }

                val view = View(context)
                view.apply {
                    layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100)
                    view.setBackgroundColor(Color.CYAN)
                }

                result.addView(image)
                layout1.addView(view)
                layout1.addView(layout)
                layout1.addView(layout2)
                result.addView(layout1)
                return TenthHolder(result)
            }

            var imageId = -1
            var titleId = -1
            var textId = -1
            var bodyId = -1
            var text2Id = -1
        }

        fun bind(data: AdapterData) {
            view.findViewById<TextView>(titleId)?.text = data.title
            view.findViewById<TextView>(bodyId)?.text = data.body
            view.findViewById<TextView>(textId)?.text = data.text1
            view.findViewById<TextView>(text2Id)?.text = data.text2
            view.findViewById<ImageView>(imageId)?.setImage(data.imgId)
        }
    }
}