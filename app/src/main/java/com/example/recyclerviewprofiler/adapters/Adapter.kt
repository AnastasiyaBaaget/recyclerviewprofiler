package com.example.recyclerviewprofiler.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewprofiler.AdapterData

abstract class Adapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    abstract var data: List<AdapterData>
}