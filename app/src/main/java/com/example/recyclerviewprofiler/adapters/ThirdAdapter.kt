package com.example.recyclerviewprofiler.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewprofiler.AdapterData
import com.example.recyclerviewprofiler.HolderType
import com.example.recyclerviewprofiler.ViewHolder
import com.example.recyclerviewprofiler.databinding.*

class ThirdAdapter(val context: Context) : Adapter() {
    override var data = listOf<AdapterData>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            HolderType.first.ordinal -> FirstHolder.create(parent)
            HolderType.second.ordinal -> SecondHolder.create(parent)
            HolderType.third.ordinal -> ThirdHolder.create(parent)
            HolderType.fourth.ordinal -> FourthHolder.create(parent)
            HolderType.fifth.ordinal -> FifthHolder.create(parent)
            HolderType.sixth.ordinal -> SixthHolder.create(parent)
            HolderType.seventh.ordinal -> SeventhHolder.create(parent)
            HolderType.eighth.ordinal -> EightHolder.create(parent)
            HolderType.ninth.ordinal -> NinthHolder.create(parent)
            else -> TenthHolder.create(parent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            HolderType.first.ordinal -> (holder as FirstHolder).bind(data[position])
            HolderType.second.ordinal -> (holder as SecondHolder).bind(data[position])
            HolderType.third.ordinal -> (holder as ThirdHolder).bind(data[position])
            HolderType.fourth.ordinal -> (holder as FourthHolder).bind(data[position])
            HolderType.fifth.ordinal -> (holder as FifthHolder).bind(data[position])
            HolderType.sixth.ordinal -> (holder as SixthHolder).bind(data[position])
            HolderType.seventh.ordinal -> (holder as SeventhHolder).bind(data[position])
            HolderType.eighth.ordinal -> (holder as EightHolder).bind(data[position])
            HolderType.ninth.ordinal -> (holder as NinthHolder).bind(data[position])
            HolderType.tenth.ordinal -> (holder as TenthHolder).bind(data[position])
        }

    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.ordinal
    }

    class FirstHolder(binding: ItemFirstVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): FirstHolder{
                val binding = ItemFirstVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return FirstHolder(binding)
            }
        }

        val vm = FirstViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class FirstViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class SecondHolder(binding: ItemSecondVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): SecondHolder{
                val binding = ItemSecondVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return SecondHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class ThirdHolder(binding: ItemThirdVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): ThirdHolder{
                val binding = ItemThirdVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ThirdHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
            binding.imageButton.setOnClickListener {
                Toast.makeText(it.context, "Hello", Toast.LENGTH_SHORT).show()
            }
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class FourthHolder(binding: ItemFourthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): FourthHolder{
                val binding = ItemFourthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return FourthHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class FifthHolder(binding: ItemFifthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): FifthHolder{
                val binding = ItemFifthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return FifthHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class SixthHolder(binding: ItemSixthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): SixthHolder{
                val binding = ItemSixthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return SixthHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class SeventhHolder(binding: ItemSeventhVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): SeventhHolder{
                val binding = ItemSeventhVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return SeventhHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class EightHolder(binding: ItemEighthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): EightHolder{
                val binding = ItemEighthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return EightHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class NinthHolder(binding: ItemNinthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): NinthHolder{
                val binding = ItemNinthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return NinthHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }

    class TenthHolder(binding: ItemTenthVmBinding): ViewHolder(binding.root) {

        companion object{
            fun create(parent: ViewGroup): TenthHolder{
                val binding = ItemTenthVmBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return TenthHolder(binding)
            }
        }

        val vm = ViewModel()

        init {
            binding.data = vm
        }

        fun bind(data: AdapterData) {
            vm.bind(data)
        }

        class ViewModel{
            val title = ObservableField<String>()
            val body = ObservableField<String>()
            val text1 = ObservableField<String>()
            val text2 = ObservableField<String>()
            val img = ObservableInt()

            fun bind(data: AdapterData) {
                title.set(data.title)
                body.set(data.body)
                text1.set(data.text1)
                text2.set(data.text2)
                data.imgId?.let {
                    img.set(data.imgId)
                }
            }
        }
    }
}