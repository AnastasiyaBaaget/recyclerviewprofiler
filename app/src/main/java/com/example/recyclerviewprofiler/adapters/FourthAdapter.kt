package com.example.recyclerviewprofiler.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewprofiler.AdapterData
import com.example.recyclerviewprofiler.HolderType

class FourthAdapter(val context: Context) : Adapter() {
    override var data = listOf<AdapterData>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            HolderType.first.ordinal -> ThirdAdapter.FirstHolder.create(parent)
            HolderType.second.ordinal -> ThirdAdapter.SecondHolder.create(parent)
            HolderType.third.ordinal -> ThirdAdapter.ThirdHolder.create(parent)
            HolderType.fourth.ordinal -> ThirdAdapter.FourthHolder.create(parent)
            HolderType.fifth.ordinal -> ThirdAdapter.FifthHolder.create(parent)
            HolderType.sixth.ordinal -> ThirdAdapter.SixthHolder.create(parent)
            HolderType.seventh.ordinal -> ThirdAdapter.SeventhHolder.create(parent)
            HolderType.eighth.ordinal -> ThirdAdapter.EightHolder.create(parent)
            HolderType.ninth.ordinal -> ThirdAdapter.NinthHolder.create(parent)
            else -> ThirdAdapter.TenthHolder.create(parent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            HolderType.first.ordinal -> (holder as ThirdAdapter.FirstHolder).bind(data[position])
            HolderType.second.ordinal -> (holder as ThirdAdapter.SecondHolder).bind(data[position])
            HolderType.third.ordinal -> (holder as ThirdAdapter.ThirdHolder).bind(data[position])
            HolderType.fourth.ordinal -> (holder as ThirdAdapter.FourthHolder).bind(data[position])
            HolderType.fifth.ordinal -> (holder as ThirdAdapter.FifthHolder).bind(data[position])
            HolderType.sixth.ordinal -> (holder as ThirdAdapter.SixthHolder).bind(data[position])
            HolderType.seventh.ordinal -> (holder as ThirdAdapter.SeventhHolder).bind(data[position])
            HolderType.eighth.ordinal -> (holder as ThirdAdapter.EightHolder).bind(data[position])
            HolderType.ninth.ordinal -> (holder as ThirdAdapter.NinthHolder).bind(data[position])
            HolderType.tenth.ordinal -> (holder as ThirdAdapter.TenthHolder).bind(data[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].type.ordinal
    }

    override fun getItemId(position: Int): Long {
        return data[position].id
    }
}